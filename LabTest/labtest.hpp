#include <list>


class ListList{


private:
	std::list<int> l;
	int l_size;


public:
	ListList(): l_size(0){
		l.clear();
	}

	ListList(int l_table[], int s){ // przyjmuje tablice elementow, ktore maja byc dodane do listy
		l_size = s;
		l.clear();
		for(int i = 0; i < s; i++){
			l.push_back(l_table[i]);
		}
	}

	void AddElementToList(int element){
		l.push_back(element);
	}

	int GetElementFromList(){ // pobiera ostatni element z list i delete go z listy
		int element;
		element = l.back();
		l.pop_back();
		return element;
	}


	int GetListSize(){
		return l_size;
	}

	bool IfListEmpty(){
		return l.empty();
	}

};
