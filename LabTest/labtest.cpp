#include "labtest.hpp"
#include "gtest.h"

TEST(ListTest, ZeroListInit){ //sprawdzenie konstruktora bez arg
	ListList ll;
	EXPECT_EQ( 0, ll.GetListSize());
	EXPECT_EQ( true, ll.IfListEmpty());
}

TEST(ListTest, ListInit){ // konstruktor z arg
	int t[] = {1, 2, 3}, s = 3;
	ListList ll( t, s);
	EXPECT_EQ( 3, ll.GetListSize());
	for(int i = (s-1); i >=0; i--){
		EXPECT_EQ(t[i], ll.GetElementFromList());
	}
}


TEST(ListTest, ListGetElement){ //sprawdzenie pobrania elementow
	ListList ll;
	ll.AddElementToList(10);
	EXPECT_EQ(10, ll.GetElementFromList());
}


int main(int argc, char **argv){

	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();

}
